package csc439team2.blackjack;

import java.util.logging.Logger;

/**
 * The Dealer object represents a Blackjack dealer.  A dealer has a Hand object to hold their cards, as well as a deck
 * so cards can be dealt, and a score that holds the sum of their hand of cards.
 * @author Kathryn Jenkins
 */
public class Dealer {
    private static final Logger logger = Logger.getLogger(Dealer.class.getName());

    public Deck deck;
    public Hand hand;
    public int score;

    /**
     * Class constructor creating a new Dealer object and instantiating it with an empty hand, a starting score of 0 and a full deck.
     */
    public Dealer() {
        logger.entering(getClass().getName(), "Dealer()");
        logger.info("Making a new Dealer");
        hand = new Hand();
        deck = new Deck();
        score = 0;
        logger.exiting(getClass().getName(), "Dealer()");
    }

    /**
     * Returns the dealer's current score as an integer.
     * @return The dealer's current score.
     */
    public int getScore() {
        logger.entering(getClass().getName(), "getScore()");
        logger.info("Getting the dealer's score...");
        logger.exiting(getClass().getName(), "getScore()");
        return score;
    }

    /**
     * Sets the dealer's score to the integer given as the parameter.
     * @param score What the dealer's score will be set to.
     */
    public void setScore(int score) {
        logger.entering(getClass().getName(), "setScore()");
        //logger.info("Setting the dealer's score...");
        this.score = score;
        logger.exiting(getClass().getName(), "setScore()");
    }

}
