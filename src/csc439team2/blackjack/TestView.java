package csc439team2.blackjack;


import java.util.logging.Logger;

/**
 * TestView extends the View abstract class.  It allows for easier testing.  TestView has a single String, tester,
 * that can be changed to use set input for the JUnit tests.
 * @author Kathryn Jenkins
 * @author Chris Chitwood
 */
public class TestView extends View {
    private static final Logger logger = Logger.getLogger(TestView.class.getName());


    private String tester = "";
    private String moveInput = "";


    /**
     * Returns the current value of the tester.
     * @return the tester value
     */
    public String getTester() {
        logger.entering(getClass().getName(), "getTester()");
        logger.info("Getting the tester value for our tests...");
        logger.exiting(getClass().getName(), "getTester()");
        return tester;
    }


    /**
     * Sets the tester to the value given as the parameter
     * @param tester what the tester will be set to
     */
    public void setTester(String tester) {
        logger.entering(getClass().getName(), "setTester()");
        logger.info("Setting the tester value for our tests...");
        logger.exiting(getClass().getName(), "setTester()");
        this.tester = tester;
    }

    /**
     * Sets the moveInput tester to the value given as the parameter
     * @param moveInput what the tester will be set to
     */
    public void setMoveInput(String moveInput) {
        logger.entering(getClass().getName(), "setMoveTester()");
        logger.info("Setting the tester value for our chosen move for our tests...");
        logger.exiting(getClass().getName(), "setMoveTester()");
        this.moveInput = moveInput;
    }

    /**
     * Returns what the Controller methods should use as input via the tester String.
     * @return the predetermined input as a String
     */
    public String getInput() {
        logger.entering(getClass().getName(), "getInput()");
        logger.info("Getting the tester value for our tests...");
        logger.exiting(getClass().getName(), "getInput()");
        return tester;
    }

    /**
     * Returns what the Controller methods should use as moveInput via the tester String.
     * @return the predetermined input as a String
     */
    public String getMoveInput(){
        logger.entering(getClass().getName(), "getMoveTester()");
        logger.info("Getting the tester value for our chosen move for our tests...");
        logger.exiting(getClass().getName(), "getMoveTester()");
        return moveInput;
    }

    /**
     * Prints an error to the console.  Functionally, printError and printMessage are the same, but
     * with a more precise naming system to make readability easier.
     * @param s the error to be printed.
     */
    public void printError(String s){
        logger.entering(getClass().getName(), "printError()");
        logger.info("A way to print errors in our test cases.");
        logger.exiting(getClass().getName(), "printError()");
    }

    /**
     * Prints a message to the console.
     * @param s the message to be printed
     */
    public void printMessage(String s){
        logger.entering(getClass().getName(), "printMessage()");
        logger.info("A way to print messages in our test cases.");
        System.out.println(s);
        logger.exiting(getClass().getName(), "printMessage()");
    }
}


