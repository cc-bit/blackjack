package csc439team2.blackjack;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

/**
 * The Shoe object holds a collections of decks.
 * The structure it uses for this in an ArrayList.
 * @author Chris Chitwood
 */
public class Shoe {
    private static final Logger logger = Logger.getLogger(Shoe.class.getName());

    /**
     * The currentShoe holds a collection of decks.
     */
    ArrayList<Deck> currentShoe = new ArrayList<>();
    private int shoeMax = 0;


    /**
     * Checks if Decks are below minimum based on the initial max setting
     * If so adds Decks until Max Decks
     * @author Patrick J. Wuerth
     */
    public void ShoeCut(){
        logger.entering(getClass().getName(), "ShoeCut");
        if(currentShoe.size() < shoeMax/5){
            logger.info("Current size is under minimum");
            while(currentShoe.size() != shoeMax){
                addDeck(new Deck());
            }
        }
        logger.exiting(getClass().getName(), "ShoeCut");
    }

    /**
     * Constructor to create a shoe holding a number of decks.
     * @param numODecks The number of decks to put into shoe.
     */
    public Shoe(int numODecks) {
        logger.entering(getClass().getName(), "Shoe");
        if (numODecks < 1)
            throw new IllegalArgumentException();
        for(int i = 0; i < numODecks; i++)
            addDeck(new Deck());
        shoeMax = numODecks;
        logger.info("Set max number of Decks");
        logger.exiting(getClass().getName(), "Shoe");
    }

    /**
     * Adds a deck to the current shoe.
     * @param currentDeck Deck to be added.
     */
    public void addDeck(Deck currentDeck) {
        logger.entering(getClass().getName(), "addDeck");
        if (currentDeck != null) {
            currentShoe.add(currentDeck);
            logger.info("Added Deck");
        }else {
            throw new IllegalArgumentException();
        }
        logger.exiting(getClass().getName(), "addDeck");
    }

    /**
     * Method to choose a card at random.
     * It then calls removeEmptyDeck().
     * Checking the if below minimum size
     */
    public Card pick()  {
        logger.entering(getClass().getName(), "pick");
        Random rand = new Random();
        int range = currentShoe.size();
        if (currentShoe.size() == 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        Card pickedCard = currentShoe.get(rand.nextInt(range)).pick();
        removeEmptyDeck();
        logger.info("Remove possible empty decks");
        ShoeCut();  //Checking the if below minimum

        logger.exiting(getClass().getName(), "pick");
        return pickedCard;
    }

    /**
     * Method to be called to remove any empty decks.
     */
    public void removeEmptyDeck() {
        logger.entering(getClass().getName(), "removeEmptyDeck");
        for (int i = 0; i < currentShoe.size(); i++) {
            if(currentShoe.get(i).size() == 0){
                currentShoe.remove(i);
            }
        }
        logger.exiting(getClass().getName(), "removeEmptyDeck");
    }

    /**
     * Returns the number of decks in the shoe.
     * @return Number of decks in collection.
     */
    public int numDecks() {
        logger.entering(getClass().getName(), "numDecks");
        logger.exiting(getClass().getName(), "numDecks");
        return currentShoe.size();
    }

    /**
     * Returns the number of cards in the shoe.
     * @return Number of cards in collection.
     */
    public int size() {
        logger.entering(getClass().getName(), "size");
        int total = 0;
        for (int i = 0; i < currentShoe.size(); i++) {
            total = total + currentShoe.get(i).size();
        }
        logger.exiting(getClass().getName(), "size");
        return total;
    }
}
