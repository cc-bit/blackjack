package csc439team2.blackjack;


import java.util.logging.Logger;

/**
 * The Player object represents a player playing Blackjack.  Every player has an integer balance that they
 * determine (from 10-5000).  Each player can also bet chips (anywhere from 10-max balance).  Players also have
 * a Hand object to represent their hand of cards.
 * @author Kathryn Jenkins
 */
public class Player {
    private static final Logger logger = Logger.getLogger(Player.class.getName());


    private int balance;
    private int bet;
    public Hand hand;
    public int score;

    /**
     * Class constructor creating a new Player object and instantiating it with an empty hand.
     */
    public Player() {
        logger.entering(getClass().getName(), "Player()");
        logger.info("Making a new player with a zero balance, bet, score and empty hand.");
        balance = 0;
        bet = 0;
        hand = new Hand();
        score = 0;
        logger.exiting(getClass().getName(), "Player()");
    }

    /**
     * Sets the number of chips the player will get.
     * @param i Number of chips to give player.
     */
    public void setBalance(int i)  {
        logger.entering(getClass().getName(), "setBalance()");
        logger.info("Setting the player's balance...");
        this.balance = i;
        logger.exiting(getClass().getName(), "setBalance()");
    }

    /**
     * Returns how many chips the player currently has.
     * @return The player's current chip balance.
     */
    public int getBalance() {
        logger.entering(getClass().getName(), "getBalance()");
        logger.info("Getting the player's balance...");
        logger.exiting(getClass().getName(), "getBalance()");
        return this.balance;
    }

    /** Returns how much the player is currently betting.
     * @return The player's current bet
     */
    public int getBet() {
        logger.entering(getClass().getName(), "getBet()");
        logger.info("Getting the player's bet...");
        logger.exiting(getClass().getName(), "getBet()");
        return bet;
    }

    /**
     * Sets how much the player's current bet is.
     * @param bet How much to set the player's bet to.
     */
    public void setBet(int bet) {
        logger.entering(getClass().getName(), "setBet()");
        logger.info("Setting the player's bet...");
        this.bet = bet;
        logger.exiting(getClass().getName(), "setBet()");
    }

    /**
     * Returns the player's score as an integer.
     * @return The player's current score.
     */
    public int getScore() {
        logger.entering(getClass().getName(), "getScore()");
        logger.info("Getting the player's score...");
        logger.exiting(getClass().getName(), "getScore()");
        return score;
    }

    /**
     * Sets the player's score to the value specified as the parameter.
     * @param score What the score will be set to.
     */
    public void setScore(int score) {
        logger.entering(getClass().getName(), "setScore()");
        //logger.info("Setting the player's score...");
        this.score = score;
        logger.exiting(getClass().getName(), "setScore()");
    }

    /**
     * Doubles players bet and sets bet to new bet
     * @author Patrick J. Wuerth
     */
    public void double_bet() {
        logger.entering(getClass().getName(), "double_bet()");
        logger.info("doubling the player's bet.");
        this.setBalance(this.getBalance() - this.getBet());
        bet *=2;
        logger.exiting(getClass().getName(), "double_bet()");
    }
    /*
    /**
     * Player losses its bet and round is over.
     * @author Patrick J. Wuerth
     */
    /*public void bust() {
        logger.entering(getClass().getName(), "bust()");
        if(balance - bet <= 0){
            logger.info("Can't have a negative bet, so let's set that to 0.");
            balance = 0;
        }else{
            logger.info("Subtracting the bet from the balance");
            //balance -= bet;
        }
        logger.info("Setting bet back to zero for next round.");
        bet = 0;
        logger.exiting(getClass().getName(), "getScore()");
    }*/

}




