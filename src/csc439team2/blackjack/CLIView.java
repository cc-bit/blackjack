package csc439team2.blackjack;

import java.util.Scanner;
import java.util.logging.Logger;


/**CLIView is the implementation of View.  It implements methods to obtain user input, print out a message to the console,
 * or print an error.
 * @author Kathryn Jenkins
 */
public class CLIView extends View {
    private static final Logger logger = Logger.getLogger(CLIView.class.getName());

    /**
     *Obtains input from the user.  If a player enters quit at any time, it will exit the game.  Otherwise, it returns the
     * input.
     * @return Input from the user.
     * @throws RuntimeException if a user enters quit
     */
    public String getInput() throws RuntimeException {
        logger.entering(getClass().getName(), "getInput()");
        logger.info("Making the scanner");
        Scanner scan = new Scanner(System.in);
        String input = "";
        try{
            logger.info("Trying to get player input");
            input = scan.next();
            if (input.toLowerCase().equals("quit")) {
                logger.info("Player entered quit, time to leave the game");
                throw new RuntimeException();
            }
        } catch (RuntimeException rtex) {
            logger.info("Exit game");
            printMessage("Thanks for playing!");
            System.exit(0);
        }
        logger.exiting(getClass().getName(), "getInput()");
        return input;
    }

    public String getMoveInput() throws RuntimeException {
        logger.entering(getClass().getName(), "getMoveInput()");
        Scanner scan = new Scanner(System.in);
        String input = "";
        try{
            logger.info("Trying to get player input");
            input = scan.next();
            if (input.toLowerCase().equals("quit")) {
                logger.info("Player entered quit, time to leave the game");
                throw new RuntimeException();
            }
        } catch (RuntimeException rtex) {
            printMessage("Thanks for playing!");
            System.exit(0);
        }
        logger.exiting(getClass().getName(), "getMoveInput()");
        return input;
    }



    /**
     * Method to print an error to the console
     * @param s the error to be printed.
     */
    public void printError(String s) {
        logger.entering(getClass().getName(), "printError()");
        System.out.println(s);
        logger.exiting(getClass().getName(), "printError()");
    }

    /**
     * Method to print a message to the console.
     * @param s the message to be printed
     */
    public void printMessage(String s) {
        logger.entering(getClass().getName(), "printMessage()");
        System.out.println(s);
        logger.exiting(getClass().getName(), "printMessage()");
    }
}




