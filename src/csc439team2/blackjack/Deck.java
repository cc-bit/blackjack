package csc439team2.blackjack;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Logger;

public class Deck {
    private static final Logger logger = Logger.getLogger(Deck.class.getName());


    /**
     * A deck contains a collection of 52 cards, 13 from each suit
     */
    ArrayList<Card> collection = new ArrayList<Card>();

    /**
     * calls the initialize function at creation
     */
    public Deck() {
        logger.entering(getClass().getName(), "Deck()");
        logger.info("Initializing the deck of cards...");
        initialize();
        logger.exiting(getClass().getName(), "Deck()");
    }

    /**
     * Initializes the Arraylist with a collection of 52 cards, 13 from each suit
     */
    public void initialize(){
        logger.entering(getClass().getName(), "initialize()");
        logger.info("Adding one of each card to the deck...");
        for(int i=1;i<=4;++i){ //4 suits
            for(int j=1;j<=13;++j){
                collection.add(new Card(i,j)); //13 from each suit
            }
        }
        logger.exiting(getClass().getName(), "initialize()");
    }

    /**
     *The Deck object should have a pick method which will choose a card from the deck at random.
     * Because we can pick at random, we do not need a shuffle method.
     * The pick method should return a card object.
     * When a card is picked from the deck, it should be removed from the collection.
     * If no cards remain in the deck, pick should throw an exception.
     * @return a random card from the collection
     * @throws IllegalArgumentException if deck is empty
     */
    public Card pick() throws IllegalArgumentException{
        logger.entering(getClass().getName(), "pick()");
        if(collection.isEmpty()){
            logger.info("If collection of cards is empty, we can't pick a card.");
            throw new IllegalArgumentException();
        }
        logger.info("Generating a random number to simulate picking out of a shuffled deck");
        Random rand = new Random();
        logger.exiting(getClass().getName(), "pick()");
        return collection.remove(rand.nextInt(collection.size()));
    }

    /**
     * The Deck object should also have a size method that returns the number of cards remaining in the deck.
     * You should just be able to have your size method call the size method on our card collection.
     * @return the size of the collection
     */
    public int size(){
        logger.entering(getClass().getName(), "size()");
        logger.exiting(getClass().getName(), "size()");
        logger.info("Getting size of the deck...");
        return collection.size();
    }
}
