package csc439team2.blackjack;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 *The Hand object represents a player's hand of playing Card objects.  The class
 * utilizes an ArrayList to easily access, add, and remove cards from the hand.
 * @author Kathryn Jenkins
 */
public class Hand {
    private static final Logger logger = Logger.getLogger(Hand.class.getName());


    public ArrayList<Card> hand;

    /**
     *Class constructor specifying a Hand object and initializing it with an empty
     * ArrayList to hold the player's cards.
     */
    public Hand() {
        logger.entering(getClass().getName(), "Hand()");
        logger.info("Making a new Hand");
        logger.exiting(getClass().getName(), "Hand()");
        hand = new ArrayList<>();
    }

    /**
     *Adds the specified card to the hand.  If a non-card object or an invalid card is attempted to be added
     * to the hand, an IllegalArgumentException is thrown.
     * @param c the card to add to the hand
     * @throws IllegalArgumentException handles any error where the program would attempt to add
     * either an invalid card or a non-card item to the hand.
     */
    public void addCard(Card c) throws IllegalArgumentException {
        logger.entering(getClass().getName(), "addCard()");
        if (c == null) {
            logger.info("Trying to add a non-card object isn't allowed.");
            throw new IllegalArgumentException("Can't add non-card objects to the hand!");
        }
        logger.info("Adding the card...");
        hand.add(c);
        logger.exiting(getClass().getName(), "addCards()");
    }

    /**
     *Returns the ArrayList of the cards in the string.
     * @return a collection of the cards in the hand.
     */
    public ArrayList<Card> getCards() {
        logger.entering(getClass().getName(), "getCards()");
        logger.info("Getting the list of cards in this hand...");
        logger.exiting(getClass().getName(), "getCards()");
        return this.hand;
    }

    /**
     *Returns the number of cards currently in the player's hand by taking the
     * current size of the Hand ArrayList.
     * @return number of cards in a player's hand
     */
    public int size() {
        logger.entering(getClass().getName(), "size()");
        logger.info("Getting the current size of this hand...");
        logger.exiting(getClass().getName(), "size()");
        return this.hand.size();
    }

    /**
     *Returns a string including a list of all cards currently in the player's
     * hand.
     * @return a string including a list of all cards currently in the player's
     * hand.
     */
    public String handToString() {
        logger.entering(getClass().getName(), "handToString()");
        logger.info("Making a StringBuilder to hold the hand...");
        StringBuilder handString = new StringBuilder();
        if (this.hand.size() == 0) {
            logger.info("The hand is currently empty.");
            return "Your hand is empty!";
        }
        else {
            for (Card c : this.hand) {
                logger.info("Adding each card to the StringBuilder...");
                handString.append(c.cardToString()).append('\n');
            }
        }
        logger.exiting(getClass().getName(), "handToString()");
        return handString.toString();
    }
}
