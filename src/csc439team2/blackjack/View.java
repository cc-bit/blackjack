package csc439team2.blackjack;

import java.util.logging.Logger;

/**
 * The View object provides abstract methods for its various subclasses.
 * @author Kathryn Jenkins
 */
public abstract class View {
    private static final Logger logger = Logger.getLogger(View.class.getName());


    /**
     * Grab's the user's input.
     * @return the user's input as a string
     */
    public String getInput() throws RuntimeException {
        logger.entering(getClass().getName(), "getInput()");
        logger.info("First hit the abstract method in the abstract View class.");
        logger.exiting(getClass().getName(), "getInput()");
        return null;
    }

    /**
     * Grab's the user's input for an int.
     * @return the user's input as a string
     */
    public String getMoveInput() throws RuntimeException {
        logger.entering(getClass().getName(), "getMoveInput()");
        logger.info("First hit the abstract method in the abstract View class.");
        logger.exiting(getClass().getName(), "getMoveInput()");
        return null;
    }

    /**
     * Prints an error to the console.  Functionally, printError and printMessage are the same, but
     * with a more precise naming system to make readability easier.
     * @param s the error to be printed.
     */
    public void printError(String s){
        logger.entering(getClass().getName(), "printError()");
        logger.info("First hit the abstract method in the abstract View class.");
        logger.exiting(getClass().getName(), "printError()");
    }

    /**
     * Prints a message to the console.
     * @param s the message to be printed
     */
    public void printMessage(String s){
        logger.entering(getClass().getName(), "printMessage()");
        logger.info("First hit the abstract method in the abstract View class.");
        logger.exiting(getClass().getName(), "printMessage()");

    }
}
