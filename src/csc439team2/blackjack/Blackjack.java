package csc439team2.blackjack;

import java.util.logging.Logger;

/**
 * The Blackjack Class represents the actual game.  It holds and runs the main method which creates a new CLIView and
 * controller and invokes the playBlackjack method on the controller to start the game.
 * @author Kathryn Jenkins
 */
public class Blackjack {
    private static final Logger logger = Logger.getLogger(Blackjack.class.getName());

    /**
     * Blackjack's entry point.
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        logger.entering(Blackjack.class.getName(), "main()");
        CLIView view = new CLIView();
        Controller controller = new Controller(new Player(), new Dealer(), view);
        try {
            logger.info("Starting the game!");
            controller.playBlackjack();
        } catch(Exception ex) {
            logger.info("If this is reached then the game really broke.");
            System.exit(1);
        }
        logger.exiting(Blackjack.class.getName(), "playerStandOrBust()");
    }

}