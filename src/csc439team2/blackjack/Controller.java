package csc439team2.blackjack;

import java.util.logging.Logger;

/**
 *The Controller object handles all logic for the blackjack game.  It has a Player, Dealer and CLIView object in order to change
 * a player's (or dealer's) values and display/receive messages to the command line.  The controller also uses a boolean (done)
 * to determine whether or not the game is finished.  The controller also has multiple booleans to allow for while loops in the game
 * to run-- buyingChips, settingBet, determiningMove, playerStands, and playerDouble.
 * @author Kathryn Jenkins
 */
public class Controller {
    private static final Logger logger = Logger.getLogger(Controller.class.getName());

    public Player player;
    public Dealer dealer;
    public View view;
    public boolean done = false;
    private boolean buyingChips = true;
    private boolean settingBet = true;
    private boolean determiningMove = true;
    private boolean playerStands = false;
    private boolean playerDouble = false;
    private int roundNum = 1;

    /**
     *Class constructor specifying a new Controller object that gets instantiated with a new Player, Dealer, and View.
     * @param player the player of the game
     * @param dealer the dealer of the game
     * @param view the object to interact with the player
     * @throws IllegalArgumentException if a param is null
     */
    public Controller(Player player, Dealer dealer, View view) {
        logger.entering(getClass().getName(), "Controller");

        if((player == null ) || ( dealer == null) || (view == null)) {
            logger.info("player, dealer, or view was null");
            throw new IllegalArgumentException();
        }

        this.player = player;
        this.dealer = dealer;
        this.view = view;
        logger.exiting(getClass().getName(), "Controller");

    }

    // Getter and setter methods for JUnit tests.

    /**
     * Getter method for tests
     * @return
     */
    public boolean isBuyingChips() {
        return buyingChips;
    }

    /**
     * Setter method for tests
     * @param buyingChips
     */
    public void setBuyingChips(boolean buyingChips) {
        this.buyingChips = buyingChips;
    }

    /**
     * Getter method for tests
     * @return settingBet flag
     */
    public boolean isSettingBet() {
        return settingBet;
    }

    /**
     * Setter method for tests.
     * @param settingBet
     */
    public void setSettingBet(boolean settingBet) {
        this.settingBet = settingBet;
    }

    /**
     * Getter method for tests
     * @return determiningMove flag
     */
    public boolean isDeterminingMove() {
        return determiningMove;
    }

    /**
     * Setter method for tests.
     * @param determiningMove
     */
    public void setDeterminingMove(boolean determiningMove) {
        this.determiningMove = determiningMove;
    }

    /**
     * Getter method for tests
     * @return playerStands flag
     */
    public boolean isPlayerStands() {
        return playerStands;
    }

    /**
     * Setter method for tests
     * @param playerStands
     */
    public void setPlayerStands(boolean playerStands) {
        this.playerStands = playerStands;
    }

    /**
     * Getter method for tests
     * @return playerDouble flag
     */
    public boolean isPlayerDouble() {
        return playerDouble;
    }

    /**
     * Getter method for tests.
     * @return roundNum
     */
    public void setPlayerDouble(boolean playerDouble) {
        this.playerDouble = playerDouble;
    }

    /**
     * Setter method for tests.
     * @param roundNum
     */
    public void setRoundNum(int roundNum) {
        this.roundNum = roundNum;
    }

    /**
     * Getter method for tests.
     * @return roundNum flag
     */
    public int getRoundNum() {
        return roundNum;
    }

    // End of getter and setter methods for JUnit tests.

    /**
     * Runs the game.  Uses a while loop to determine if the player is "done" with the game or not.  The player will
     * buy an amount of chips (max of 5000), place a bet and the player and dealer will both be dealt two cards.  One of
     * the dealer's cards will be hidden.  The player will be asked what they would like to do.  A player can double, hit,
     * stand or quit.  If a player goes over 21, they lose.  If the dealer goes over 21, they win.
     */
    public void playBlackjack() {
        logger.entering(getClass().getName(), "playBlackjack()");
        while (!done) {
            if(roundNum == 1) {
                view.printMessage("Welcome to our implementation of blackjack!");

                //// This section can be put into its own method later if we want to allow buying more chips.
                while (buyingChips) {
                    try {
                        logger.info("Setting the Player Balance.");
                        player.setBalance(buyChips());
                    } catch (NumberFormatException nfex) {
                        logger.info("A NumberFormatException was thrown by player.setBalance(buyChips())");
                        view.printError("Sorry, this was supposed to be a number");
                        continue;
                    } catch (IllegalArgumentException ilex) {
                        logger.info("IllegalArgumentException thrown by player.setBalance(buyChips())");
                        view.printError("Sorry, you can only buy between 10 and 5000 chips.");
                        continue;
                    }
                    buyingChips = false;
                }
                ////
                view.printMessage("You received " + player.getBalance() + " chips.");
                logger.info("Setting the Player's bet.");
                setTheBet();
                dealHand(2, 2, 1);
                determineTheMove();
            }
            else
                done = true;
        }
        logger.exiting(getClass().getName(), "playBlackjack()");
    }

    /**
     * Method to call placeBet() and player.setBet() while catching the exceptions they throw.
     */
    public void setTheBet() {
        logger.entering(getClass().getName(), "setTheBet()");
        while(settingBet) {
            try {
                logger.info("Setting the Player's bet.");
                player.setBet(placeBet());
                player.setBalance(player.getBalance() - player.getBet());
                settingBet = false;
            } catch(NumberFormatException nfex){
                logger.info("NumberFormatException was thrown by player.setBet(placeBet()");
                view.printError("Sorry, this was supposed to be a number.");
            } catch(IllegalArgumentException ilex){
                logger.info("IllegalArgumentException was thrown by player.setBet(placeBet()");
                view.printError("Sorry, you can only bet between 10 and " + determineMaxBet() + " chips.");
            }
        }
        view.printMessage("You bet " + player.getBet() + " chips.");
        view.printMessage("Current balance: " + player.getBalance());
        logger.exiting(getClass().getName(), "setTheBet()");
    }

    /**
     * Method to call determinePlayerMove() while catching the exceptions it throws.
     */
    public void determineTheMove() {
        logger.entering(getClass().getName(), "determineTheMove");
        while(determiningMove) {
            try {
                determinePlayerMove();
                determiningMove = false;

            } catch (IllegalArgumentException ilex) {
                logger.info("IllegalArgumentException was thrown by determinePlayerMove()");
                view.printError("Sorry, that is not a valid move.");
            }
        }
        logger.exiting(getClass().getName(), "determineTheMove");

    }



    /**
     * Asks the player how many chips they would like to buy (between 10 and 5000) and sets the player's chip balance to
     * that number.
     * @return number of chips a player wants to buy
     */
    public int buyChips() {
        logger.entering(getClass().getName(), "buyChips()");
        boolean waiting = true;
        int temp = 0;
        while (waiting) {
            view.printMessage("How many chips do you want to buy? (Max: 5000)");
            logger.info("Waiting for player input.");
            String input = view.getInput();
            temp = Integer.parseInt(input);
            if (temp < 10 || temp > 5000) {
                logger.info("IllegalArgument thrown due to the input being outside given range.");
                throw new IllegalArgumentException();
            }
            waiting = false;
        }
        logger.exiting(getClass().getName(), "buyChips()");
        return temp;
    }

    /**
     * Asks a player how many chips they would like to bet (up to 500) and sets the player's current bet to that value.
     * @return number of chips a player wants to bet
     */
    public int placeBet() {
        logger.entering(getClass().getName(), "placeBet()");
        boolean waiting = true;
        int temp = 0;
        while (waiting) {

            if(player.getBalance() < 10)
                view.printMessage("You don't have enough chips.  All you can do now is \"quit\"");
            view.printMessage("Round: "+ roundNum);
            view.printMessage("Current balance: " + player.getBalance());
            if(player.getBalance() == 0)
                view.printMessage("You are out of chips.  All you can do now is \"quit\"");
            else
                view.printMessage("How much would you like to bet? Min: " + Math.min(10, player.getBalance()) + " Max: " + determineMaxBet());
            logger.info("Waiting for player input.");
            String input = view.getInput();

            temp = Integer.parseInt(input);
            if(temp < 10 || temp > 500 || temp > player.getBalance()) {
                logger.info("IllegalArgument thrown due to the input being outside given range.");
                throw new IllegalArgumentException();
            }
            waiting = false;
        }
        logger.exiting(getClass().getName(), "placeBet()");
        return temp;
    }

    /**
     * Determines the player's maximum allowed bet.  If a player has >500 chips, they can bet between 10 and 500 chips.  If they
     * have below 500 chips, they can bet between 10 and the max number of chips they currently have.
     * @return the player's maximum bet
     */
    public int determineMaxBet() {
        logger.exiting(getClass().getName(), "determineMaxBet()");
        logger.entering(getClass().getName(), "determineMaxBet()");
        return Math.min(player.getBalance(), 500);
    }


    /**
     * Adds a number of cards to both the dealer and the player's hands, determines the player's current score, and prints
     * both the player and dealer's hands.
     * @param giveToPlayer how many cards to give to the player
     * @param giveToDealer how many cards to give to the dealer
     * @param numToHide how many of the dealer's cards that need to be face down
     */
    public void dealHand(int giveToPlayer, int giveToDealer, int numToHide) {
        logger.entering(getClass().getName(), "dealHand()");
        try {
            if (giveToPlayer < 0 || giveToDealer < 0 || numToHide < 0) {
                logger.info("One or more parameters to dealHand() were less than 0");
                throw new Exception();
            }
            for (int i = 0; i < giveToPlayer; i++) {
                logger.info("Player is dealt a card.");
                player.hand.addCard(dealer.deck.pick());
            }
            for (int i = 0; i < giveToDealer; i++) {
                logger.info("Dealer draws card.");
                dealer.hand.addCard(dealer.deck.pick());
            }
            for (int i = 0; i < numToHide; i++) {
                logger.info("Face down set.");
                dealer.hand.hand.get(i).faceDown = true;
            }
        }catch(Exception ex) {
            view.printError("Sorry, something went wrong with the game.");
        }
        determinePlayerScore();
        printBothHands();

        logger.exiting(getClass().getName(), "dealHand()");
    }

    /**
     * Prints both the player's and the dealer's hands to the console.  If any of the dealer's cards
     * have their faceDown boolean set to true, the actual card value will not be shown, and instead
     * "This card is face down" will be printed.
     */
    public void printBothHands() {
        logger.entering(getClass().getName(), "printBothHands()");

        view.printMessage("Player Hand:\n" + player.hand.handToString() + "\nDealer Hand:");
        for (Card c : dealer.hand.hand) {
            if (c.faceDown) {
                view.printMessage("This card is face down.");
            }
            else {
                view.printMessage(c.cardToString());
            }
        }
        logger.exiting(getClass().getName(), "printBothHands()");

    }

    /**
     * Determines the player's next move based on their input.  If the player enters anything other than quit, hit, stand, or
     * double, the game will ask again.  If a player enters quit, exit game.  If double, and if conditions are met, double the
     * bet and hit.  If hit, draw a card and see if they lost.  If stand, the dealer will draw.
     */
    public void determinePlayerMove() {
        logger.entering(getClass().getName(), "determinePlayerMove()");
        playerDouble = false;
        boolean waiting = true;
        while (waiting) {
            view.printMessage("\nYour current score is: " + player.getScore());
            view.printMessage("\nWhat will you do? You can hit, stand, double, or quit.");
            String input = view.getMoveInput().toLowerCase();
            waiting = false;
            logger.info("Checking if input is invalid.");
            if (!(input.equals("quit") || input.equals("hit") || input.equals("stand") || input.equals("double"))) {
                throw new IllegalArgumentException();
            }
//            logger.info("Checking if player balance is less than 10.");
//            if (player.getBalance() < 10) {
//                view.printMessage("Your balance is less than 10 all you can do is \"quit\"");
//                throw new IllegalArgumentException();
//            }

            if ((input.equals("double"))) {
                logger.info("Checking if doubling is currently allowed.");
                if (player.hand.size() != 2 || (player.getScore() < 9 || player.getScore() > 11)) {
                    view.printMessage("You cannot double at this time.");
                    throw new IllegalArgumentException();
                }
                playerDouble = true;
                player.double_bet();
                view.printMessage("Your bet is now " + player.getBet() + " chips.\n");
                dealHand(1, 0, 0); //hit the player one card
                playerStandOrBust();
            }
            else if ((input.equals("stand"))) {
                playerStands = true;
                dealerHit();
                determineWinner();
                newRound();

            }
            else if (input.equals("hit")) {
                dealHand(1, 0, 0);
                determinePlayerScore();
                playerStandOrBust();

            }
        }
        logger.exiting(getClass().getName(), "determinePlayerMove()");

    }


    /**
     * After the player takes one hit, he then either busts or stands.
     * @author Patrick J. Wuerth
     */
    public void playerStandOrBust(){
        logger.entering(getClass().getName(), "playerStandOrBust()");

        if(player.getScore() < 21){
            determinePlayerMove();  //Player stand, game moves on
        }
        else if (player.getScore() > 21){
            logger.info("Player Busted. Updating player accordingly.");
            //player.bust();
            //view.printMessage("\nCurrent Score: " + player.getScore() + "\nYou lost this round!\nCurrent Balance: " + player.getBalance() + "\n");
            determineWinner();
            newRound();
        }
        else {
            logger.info("Start new round");
            view.printMessage("Blackjack!\n");
            determineWinner();
            newRound();
        }
        logger.exiting(getClass().getName(), "playerStandOrBust()");

    }

    /**
     * Determines the player's current score dependent on their current hand.
     * For each card in the player's hand, if a card is a face card, the player gains 10 to their score. 
     * If the card is between 2 and 10 (inclusive), the player gains the respective points to the card number. 
     * If the card is an ace, first add 11.  If that pushes the player over 21, it instead adds 1.
     * @return the player's current score
     */
    public int determinePlayerScore(){
        logger.entering(getClass().getName(), "determinePlayerScore()");
        logger.info("Setting Player score...");
        player.setScore(0);
        for(Card c : player.hand.hand) {
            if(c.getNumber() == 11 || c.getNumber() == 12 || c.getNumber() == 13) {
                logger.info("Card was Jack, Queen, or King.  Plus 10 points");
                player.setScore(player.getScore() + 10);
            }
            else if (c.getNumber() <= 10 && c.getNumber() >= 2) {
                player.setScore(player.getScore() + c.getNumber());
            }
            else if (c.getNumber() == 1) {
                logger.info("Card was an Ace.  Plus 11 points.");
                player.setScore(player.getScore() + 11);
                if (player.getScore() > 21) {
                    logger.info("Player busted.  Applying penalty");
                    player.setScore(player.getScore() - 10);
                }
            }
        }
        logger.exiting(getClass().getName(), "determinePlayerScore()");
        return player.getScore();
    }

    /**
     * Determines the Dealer's current score.  Same as determinePlayerScore().
     * @return the dealer's current score
     */
    public int determineDealerScore(){
        logger.entering(getClass().getName(), "determineDealerScore()");
        logger.info("Setting Dealer score...");
        dealer.setScore(0);
        for(Card c : dealer.hand.hand) {
            if(c.getNumber() == 11 || c.getNumber() == 12 || c.getNumber() == 13) {
                logger.info("Card was Jack, Queen, or King.  Plus 10 points");
                dealer.setScore(dealer.getScore() + 10);
            }
            else if (c.getNumber() <= 10 && c.getNumber() >= 2) {
                dealer.setScore(dealer.getScore() + c.getNumber());
            }
            else if (c.getNumber() == 1) {
                logger.info("Card was an Ace.  Plus 11 points.");
                dealer.setScore(dealer.getScore() + 11);
                if (dealer.getScore() > 21) {
                    logger.info("Dealer busted.  Applying penalty");
                    dealer.setScore(dealer.getScore() - 10);
                }
            }
        }
        logger.exiting(getClass().getName(), "determineDealerScore()");
        return dealer.getScore();
    }

    /**
     * The dealer draws cards until their score is 17 or higher.
     */
    public void dealerHit() {
        logger.entering(getClass().getName(), "dealerHit()");

        if (playerStands) {
            while(dealer.score < 17) {
                view.printMessage("Dealer Hits");
                logger.info("Dealer is drawing.");
                dealer.hand.addCard(dealer.deck.pick());
                dealer.setScore(determineDealerScore());
                view.printMessage("Dealer Score is now: " + dealer.getScore());
            }
        }
        logger.exiting(getClass().getName(), "dealerHit");
    }

    /**
     * Determines who wins.  Players who have a score of 21 or less while having a score higher than the dealer win.
     * Players also win if they have a score of 21 or less and the dealer exceeds 21.
     * It returns a 1 for the player winning or a 0 for the dealer winner.
     * @return integer for player or dealer
     */
    public int determineWinner() {
        logger.entering(getClass().getName(), "determineWinner()");
        if (player.getScore() >= 0 && player.getScore() <= 21 && player.getScore() > dealer.getScore()) {
            logger.info("Player won because they had less than 21 and had more than the dealer.");

            player.setBalance(player.getBalance() + 2 * player.getBet());
            if (playerDouble) {
                player.setBalance(player.getBalance() + (player.getBet()));
            }
            view.printMessage("\nCurrent Score: " + player.getScore() + "\nDealer Score: " + dealer.getScore() + "\nYou won this round!\nCurrent Balance: " + player.getBalance() + "\n");
            logger.exiting(getClass().getName(), "determineWinner()");
            return 1;
        } else if (player.getScore() >= 0 && player.getScore() <= 21 && dealer.getScore() > 21) {
            logger.info("Player won because only the dealer busted.");
            player.setBalance(player.getBalance() + 2 * player.getBet());
            if (playerDouble) {
                player.setBalance(player.getBalance() + (player.getBet()));

            }
            view.printMessage("\nCurrent Score: " + player.getScore() + "\nDealer Score: " + dealer.getScore() + "\nYou won this round!\nCurrent Balance: " + player.getBalance() + "\n");
            logger.exiting(getClass().getName(), "determineWinner()");
            return 1;
        } else if (player.getScore() > 21) {
            logger.info("Player lost because they busted.");
            //player.bust();
            if (playerDouble) {
                player.setBalance(player.getBalance() - player.getBet());
            }
            view.printMessage("\nCurrent Score: " + player.getScore() + "\nYou Busted!" + "\nYou lost this round!\nCurrent Balance: " + player.getBalance() + "\n");
            logger.exiting(getClass().getName(), "determineWinner()");
            return 0;
        } else if (player.getScore() >=0 && player.getScore() < 21 && dealer.getScore() == 21) {
            logger.info("Player lost because they had less than 21 and the dealer had more than the player.");
            if (playerDouble) {
                player.setBalance(player.getBalance() - player.getBet());
            }
            view.printMessage("\nCurrent Score: " + player.getScore() + "\nDealer Score: " + dealer.getScore() + "\nYou lost this round!\nCurrent Balance: " + player.getBalance() + "\n");
            logger.exiting(getClass().getName(), "determineWinner()");
            return 0;
        } else if (player.getScore() == dealer.getScore()) {
            logger.info("Player lost because the player's and dealer's scores tied.");
            if (playerDouble) {
                player.setBalance(player.getBalance() - player.getBet());
            }
            view.printMessage("Player and Dealer Tied.");
            view.printMessage("\nCurrent Score: " + player.getScore() + "\nDealer Score: " + dealer.getScore()  + "\nYou lost this round!\nCurrent Balance: " + player.getBalance() + "\n");
            logger.exiting(getClass().getName(), "determineWinner()");
            return 0;
        } else if (player.getScore() >= 0 && (player.getScore() < dealer.getScore())) {
            logger.info("Player lost because the player had a lower score than the dealer.");
            if (playerDouble) {
                player.setBalance(player.getBalance() - player.getBet());
            }
            view.printMessage("\nCurrent Score: " + player.getScore() + "\nDealer Score: " + dealer.getScore() + "\nYou lost this round!\nCurrent Balance: " + player.getBalance() + "\n");
            logger.exiting(getClass().getName(), "determineWinner()");
            return 0;
        } else {
            logger.info("Hit an undefined game result.");
            //view.printMessage("\n ELSE CONDITION HIT IN determineWinner()\nPlayer Score: " + player.getScore() + " \nDealer Score: " + dealer.getScore());
            view.printMessage("Undefined ");
            logger.exiting(getClass().getName(), "determineWinner()");
            return 0;
        }

    }


    /**
     * Method to start a new round.
     */
    public void newRound() {
        logger.entering(getClass().getName(), "newRound()");

        roundNum++;

        logger.info("Clearing hands.");
        player.hand.hand.clear();
        dealer.hand.hand.clear();

        logger.info("Resetting Scores");
        player.setScore(0);
        dealer.setScore(0);

        logger.info("Asking for the next bet.");
        settingBet = true;
        setTheBet();

        logger.info("Dealing new hands");
        dealHand(2, 2, 1);

        logger.info("Asking for next move.");
        determineTheMove();

        logger.exiting(getClass().getName(), "newRound()");

    }


}
