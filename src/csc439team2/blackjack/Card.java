package csc439team2.blackjack;

import java.util.Map;
import java.util.logging.Logger;


/**
 * The Card object represents a playing card from a standard deck of cards.
 * There are four suits (Hearts, Diamonds, Spades, and Clubs) as well as 13
 * different types of cards (Ace, 2-10, Jack, Queen, and King) each represented
 * as a Map and each assigned a numerical key (does not necessarily correspond to
 * the card's numerical value-- an Ace is assigned the key 1, but can either be a 1
 * or 11 in the game blackjack.  The Card class takes these integer keys and uses methods
 * to convert them to the correct suit/card value.
 * @author Kathryn Jenkins
 * @version 1.0
 */
public class Card {
    private static final Logger logger = Logger.getLogger(Card.class.getName());


    final Map<Integer, String> CARD_SUIT = Map.of(
            1, "Hearts",
            2, "Diamonds",
            3, "Spades",
            4, "Clubs"
    );

    final Map<Integer, String> CARD_NUMBER = Map.ofEntries(
            Map.entry(1, "Ace"),
            Map.entry(2, "Two"),
            Map.entry(3, "Three"),
            Map.entry(4, "Four"),
            Map.entry(5, "Five"),
            Map.entry(6, "Six"),
            Map.entry(7, "Seven"),
            Map.entry(8, "Eight"),
            Map.entry(9, "Nine"),
            Map.entry(10, "Ten"),
            Map.entry(11, "Jack"),
            Map.entry(12, "Queen"),
            Map.entry(13, "King")
    );

    private int suit;
    private int number;
    boolean faceDown = false;

    /**
     * Class constructor specifying a new Card object and initializing it
     * with a specified suit and number.  If either the suit or number
     * does not match a key in the specified maps, the constructor will
     * throw an IllegalArgumentException.
     * @param suit suit of the playing Card.
     * @param number numerical value of the playing Card.
     * @throws IllegalArgumentException If card suit or card number are invalid
     */
    public Card(int suit, int number) throws IllegalArgumentException {
        logger.entering(getClass().getName(), "Card()");
        //logger.info("Checking for invalid parameters...");
        if (!CARD_SUIT.containsKey(suit) || !CARD_NUMBER.containsKey(number)) {
            throw new IllegalArgumentException();
        }
        else {
            this.suit = suit;
            this.number = number;
        }
        logger.exiting(getClass().getName(), "Card()");
    }

    /**
     * Returns the suit of the card as an integer to be used as a key in the
     * CARD_SUIT map.
     * @return the key value of the suit of the card
     */
    public int getSuit() {
        logger.entering(getClass().getName(), "getSuit()");
        //logger.info("Getting this Card's Suit...");
        logger.exiting(getClass().getName(), "getSuit()");
        return this.suit;
    }

    /**
     * Returns the numerical value of the card as an integer to be used as a key in the
     * CARD_NUMBER map.
     * @return the key value of the number of the card
     */
    public int getNumber() {
        logger.entering(getClass().getName(), "getNumber()");
        //logger.info("Getting this Card's Number...");
        logger.exiting(getClass().getName(), "getNumber()");
        return this.number;
    }

    /**
     * Compares the numerical key values of two cards.  If two cards have an equal key (and therefore, have the same
     * numerical value) return true; else false.
     * @param card2 the second card to be compared to.
     * @return True if two cards are equal, else false
     */
    public boolean compareCards(Card card2) {
        logger.entering(getClass().getName(), "compareCards()");
        logger.info("Comparing two cards...");
        logger.exiting(getClass().getName(), "compareCards()");
        return (this.getNumber() == card2.getNumber());

    }

    /**
     * Grabs a card's suit key and searches the CARD_SUIT map for the specific key, and then returns
     * the String value.
     * @return String value of the card's suit
     */
    public String suitToString() {
        logger.entering(getClass().getName(), "suitToString()");
        logger.info("Returning this Card's suit by converting the int suit to the corresponding value in the map");
        logger.exiting(getClass().getName(), "suitToString()");
        return CARD_SUIT.get(this.getSuit());

    }

    /**
     * Grabs a card's numerical key and searches the CARD_NUMBER map for the specific key, and then returns
     * the String value.  For example, if a card has the number 1, the method will return "Ace".
     * @return String value of the card's number
     */
    public String numberToString() {
        logger.entering(getClass().getName(), "numberToString()");
        logger.info("Returning this Card's number by converting the int number to the corresponding value in the map");
        logger.exiting(getClass().getName(), "numberToString()");
        return CARD_NUMBER.get(this.getNumber());
    }

    /**
     * Combines a more readable interpretation of a card.  Prints the card's String value of their number followed
     * by the card's String value of the suit.  For example, Card(1, 11) would return "Jack of Hearts".
     * @return String value of a card's number and suit.
     */
    public String cardToString() {
        logger.entering(getClass().getName(), "cardToString()");
        logger.info("Returning the Card's number and suit by grabbing it from the map.");
        logger.exiting(getClass().getName(), "cardToString()");
        return this.numberToString() + " of " + this.suitToString();
    }
}
