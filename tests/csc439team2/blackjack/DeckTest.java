package csc439team2.blackjack;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DeckTest {

    @Test
    public void test_size() {
        Deck d = new Deck();
        assertEquals(52, d.size());
        d.pick();
        assertEquals(51, d.size());
    }

    @Test (expected = IllegalArgumentException.class)
    public void test_pick() {
        Deck d = new Deck();
        for(int i=1;i<=53;++i){
            d.pick();
        }
    }
}
