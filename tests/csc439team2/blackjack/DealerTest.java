package csc439team2.blackjack;

import org.junit.Test;

import static org.junit.Assert.*;

public class DealerTest {

    @Test
    public void getScore() {
        Dealer dealer = new Dealer();
        dealer.setScore(5);
        assertEquals(5, dealer.getScore());
    }

    @Test
    public void setScore() {
        Dealer dealer = new Dealer();
        dealer.setScore(5);
        assertEquals(5, dealer.getScore());
    }
}