package csc439team2.blackjack;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {

    @Test
    public void setBalance() {
        Player player = new Player();
        player.setBalance(500);
        assertEquals(500, player.getBalance());
    }

    @Test
    public void getBalance() {
        Player player = new Player();
        player.setBalance(500);
        assertEquals(500, player.getBalance());
    }

    @Test
    public void getBet() {
        Player player = new Player();
        player.setBet(100);
        assertEquals(100, player.getBet());
    }

    @Test
    public void setBet() {
        Player player = new Player();
        player.setBet(100);
        assertEquals(100, player.getBet());
    }

    @Test
    public void getScore() {
        Player player = new Player();
        assertEquals(0, player.getScore());
    }

    @Test
    public void setScore() {
        Player player = new Player();
        player.setScore(5);
        assertEquals(5, player.getScore());
    }

    @Test
    public void double_bet() {
        Player player = new Player();
        player.setBet(100);
        player.double_bet();
        assertEquals(200, player.getBet());
    }

    /*@Test
    public void bust() {
        Player player = new Player();
        player.setBalance(500);
        player.setBet(100);
        //player.bust();
        assertEquals(400, player.getBalance());
        assertEquals(0, player.getBet());
    }

    @Test
    public void bust_lowerBound() {
        Player player = new Player();
        player.setBalance(100);
        player.setBet(200);
        //player.bust();
        assertEquals(0, player.getBalance());
        assertEquals(0, player.getBet());
    }*/
}