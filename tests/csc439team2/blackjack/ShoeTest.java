package csc439team2.blackjack;

import org.junit.Test;

import static org.junit.Assert.*;

public class ShoeTest {

    @Test(expected = IllegalArgumentException.class)
    public void shoeWithLessThanOne() {
        Shoe shoe = new Shoe(0);
    }

    // Check that the object returned by Shoe.pick() is, in fact, a Card.
    @Test
    public void pick() {
        Shoe shoe = new Shoe(5);
        assertTrue(shoe.pick() instanceof Card);

    }


    // Check an exception is thrown when there are no decks to pick cards from.
    @Test (expected = IndexOutOfBoundsException.class)
    public void pick_illegal_check() {
        Shoe shoe = new Shoe(1);

        // This will pick cards from a deck until empty.
        int cardsInDeck = 52;
        for(int i = 0; i < cardsInDeck; i++) {
            shoe.currentShoe.get(1).pick();
        }
        shoe.pick();
    }


    // Check that the number of decks increase.
    @Test
    public void addDeck() {
        Shoe shoe = new Shoe(5);
        shoe.addDeck(new Deck());
        assertEquals(6,shoe.numDecks());
    }


    // Check that an exception is thrown if add null value as a deck is attempted.
    @Test (expected = IllegalArgumentException.class)
    public void addDeck_illegal_check() {
        Shoe shoe = new Shoe(5);
        shoe.addDeck(null);
        assertEquals(6,shoe.numDecks());
    }

    // Check that the number of cards match what would be in a given number of decks.
    @Test
    public void size() {
        int numODecks = 5; // Number of decks to check.
        Shoe shoe = new Shoe(numODecks);
        assertEquals(52 * numODecks,shoe.size());
    }

    // Check that after a deck is emptied it will be removed by removeEmptyDeck()
    @Test
    public void removeEmptyDeck() {
        int numODecks = 5;
        int cardsInDeck = 52;
        Shoe shoe = new Shoe(numODecks);

        // This will pick cards from a deck until empty.
        for(int i = 0; i < cardsInDeck; i++) {
            shoe.currentShoe.get(1).pick();
        }

        // Should then remove the empty deck.
        shoe.removeEmptyDeck();
        assertEquals(numODecks - 1, shoe.numDecks());
    }

    @Test (expected = ArrayIndexOutOfBoundsException.class)
    public void checkEmptyShoe() {
        int numODecks = 1;
        int cardsInDeck = 52;
        Shoe shoe = new Shoe(numODecks);

        // This will pick cards from a deck until empty.
        for(int i = 0; i < cardsInDeck; i++) {
            shoe.currentShoe.get(0).pick();
        }

        // Should then remove the empty deck.
        shoe.removeEmptyDeck();
        shoe.pick();
    }

    @Test
    public void shoeCut(){
        Shoe shoe = new Shoe(10);
        shoe.currentShoe = (new java.util.ArrayList<Deck>());//Sets numODecks to 0
        shoe.ShoeCut();//Adds Decks back to 10
        assertEquals(10, shoe.numDecks());
    }




}