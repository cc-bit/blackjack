package csc439team2.blackjack;

import org.junit.Test;

import static org.junit.Assert.*;

public class CardTest {

    @Test
    public void getSuit() {
        Card cardTest = new Card(1, 10);
        assertEquals(1, cardTest.getSuit());
    }

    @Test
    public void getNumber() {
        Card cardTest = new Card(1, 10);
        assertEquals(10, cardTest.getNumber());
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructorFalseSuitTest() {
        Card cardTest = new Card(20, 10);
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructorFalseNumberTest() {
        Card cardTest = new Card(2, 36);
    }

    @Test (expected = IllegalArgumentException.class)
    public void constructorBothFalseTest() {
        Card cardTest = new Card(10, 88);

    }

    @Test
    public void compareCardsFalse() {
        Card cardA = new Card(2, 3);
        Card cardB = new Card(3, 7);
        assertFalse("false", cardA.compareCards(cardB));
    }

    @Test
    public void compareCardsTrue() {
        Card cardA = new Card(1, 4);
        Card cardB = new Card(1, 4);
        assertTrue("true", cardA.compareCards(cardB));
    }

    @Test
    public void suitToString() {
        Card cardTest = new Card(3, 7);
        assertEquals("Spades", cardTest.suitToString());
    }

    @Test
    public void numberToString() {
        Card cardTest = new Card(1, 1);
        assertEquals("Ace", cardTest.numberToString());
    }

    @Test
    public void cardToString() {
        Card cardTest = new Card(4, 13);
        assertEquals("King of Clubs", cardTest.cardToString());
    }
}