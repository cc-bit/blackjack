package csc439team2.blackjack;

import org.junit.Test;

import static org.junit.Assert.*;

public class HandTest {

    @Test (expected = IllegalArgumentException.class)
    public void addCard() {
        Hand hand = new Hand();
        Card c1 = null;
        hand.addCard(c1);
    }

    @Test
    public void getCards() {
        Hand hand = new Hand();
        Card c1 = new Card(1, 1);
        Card c2 = new Card(2, 2);
        hand.addCard(c1);
        hand.addCard(c2);
        assertEquals(hand.hand, hand.getCards());
    }

    @Test
    public void size() {
        Hand hand = new Hand();
        Card c1 = new Card(1, 1);
        Card c2 = new Card(2, 2);
        hand.addCard(c1);
        hand.addCard(c2);
        assertEquals(2, hand.size());
    }

    /*@Test
    public void emptySize() {
        Hand hand = new Hand();
        assertEquals(0, hand.size());
    }*/

    @Test
    public void EmptyHandToString() {
        Hand hand = new Hand();
        assertEquals("Your hand is empty!", hand.handToString());
    }

    @Test
    public void handToString() {
        Hand hand = new Hand();
        Card c1 = new Card(1, 1);
        Card c2 = new Card(2, 2);
        hand.addCard(c1);
        hand.addCard(c2);
        assertEquals("Ace of Hearts" + '\n' + "Two of Diamonds" + '\n', hand.handToString());
    }
}