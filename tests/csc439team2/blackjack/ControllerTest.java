package csc439team2.blackjack;

import org.junit.Rule;
import org.junit.Test;
import org.junit.internal.runners.statements.FailOnTimeout;
import org.junit.rules.ExpectedException;
import org.junit.rules.Timeout;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.junit.runners.model.Statement;
import org.junit.runners.model.TestTimedOutException;


import java.util.concurrent.TimeoutException;

import static org.junit.Assert.*;

public class ControllerTest {

    // Rule for testing the infinite loops that we want when the player gives invalid inputs.
    @Rule
    public Timeout timeout = new Timeout(100) {
        public Statement apply(Statement base, Description description) {
            return new FailOnTimeout(base, 100) {
                @Override
                public void evaluate() throws Throwable {
                    try {
                        super.evaluate();
                        throw new TimeoutException();
                    } catch (Exception e) {}
                }
            };
        }
    };

    // Test null cases
    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithAllNull() {
        Dealer dealer = null;
        Player player = null;
        CLIView view = null;
        Controller controller = new Controller(player,dealer,view);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullPlayer() {
        Dealer dealer = new Dealer();
        Player player = null;
        CLIView view = new CLIView();
        Controller controller = new Controller(player,dealer,view);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullDealer() {
        Dealer dealer = null;
        Player player = new Player();
        CLIView view = new CLIView();
        Controller controller = new Controller(player,dealer,view);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullCLIView() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        CLIView view = null;
        Controller controller = new Controller(player,dealer,view);
    }

    @Test
    public void testCorrectConstructor() {
        Controller controller = new Controller(new Player(), new Dealer(), new CLIView());
    }

    @Test
    public void determineMaxBet() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        CLIView view = new CLIView();
        Controller controller = new Controller(player, dealer, view);
        player.setBalance(5000);
        assertEquals(500, controller.determineMaxBet());
    }

    @Test
    public void determineMaxBetFromSmallBalance() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        CLIView view = new CLIView();
        Controller controller = new Controller(player, dealer, view);
        player.setBalance(300);
        assertEquals(300, controller.determineMaxBet());
    }


    @Test
    public void dealHandPlayerSize() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        Controller controller = new Controller(player, dealer, new CLIView());
        controller.dealHand(1, 1, 0);
        assertEquals(1, controller.player.hand.hand.size());
    }

    @Test
    public void dealHandDealerSize() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        Controller controller = new Controller(player, dealer, new CLIView());
        controller.dealHand(1, 1, 0);
        assertEquals(1, dealer.hand.size());
    }

    @Test
    public void dealHandDealerFaceDownFalse() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        Controller controller = new Controller(player, dealer, new CLIView());
        controller.dealHand(1, 1, 0);
        assertFalse("false", dealer.hand.hand.get(0).faceDown);
    }

    @Test
    public void dealHandDealerFaceDownTrue() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        Controller controller = new Controller(player, dealer, new CLIView());
        controller.dealHand(1, 1, 1);
        assertTrue("true", dealer.hand.hand.get(0).faceDown);
    }

    @Test
    public void negativeDealHand() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        Controller controller = new Controller(player, dealer, new CLIView());
        controller.dealHand(-1, 1, 1);
    }

    @Test
    public void printBothHands() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        Controller controller = new Controller(player, dealer, new CLIView());
        player.hand.addCard(new Card(1, 1));
        dealer.hand.addCard(new Card(2, 2));
        controller.printBothHands();
    }

    @Test
    public void printBothHandsFaceDownDealer() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        Controller controller = new Controller(player, dealer, new CLIView());
        player.hand.addCard(new Card(1, 1));
        dealer.hand.addCard(new Card(2, 2));
        dealer.hand.addCard(new Card(3, 6));
        dealer.hand.hand.get(0).faceDown = true;
        controller.printBothHands();
    }

    @Test
    public void buyChipsOKNumber() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("500");
        assertEquals(500, c.buyChips());
    }

    @Test(expected = IllegalArgumentException.class)
    public void buyChipsCheckNeg() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("-10");
        player.setBalance(c.buyChips());
        assertEquals(0,player.getBalance());
    }

    @Test(expected = IllegalArgumentException.class)
    public void buyChipsCheckOver() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("5001");
        player.setBalance(c.buyChips());
        assertEquals(0,player.getBalance());
    }

    @Test(expected = NumberFormatException.class)
    public void buyChipsCheckIfLetter() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("b");
        player.setBalance(c.buyChips());
        assertEquals(0,player.getBalance());
    }

    @Test
    public void placeBetWith0Chips() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("500");
        player.setBalance(0);
        player.setBet(c.placeBet());
        assertEquals(0, player.getBalance());
    }

    @Test(expected = NumberFormatException.class)
    public void placeBetWithLetter() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("b");
        player.setBet(c.placeBet());
        assertEquals(0, player.getBet());
    }

    @Test(expected = IllegalArgumentException.class)
    public void placeBetWithNeg() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("-1");
        player.setBet(c.placeBet());
        assertEquals(0, player.getBet());
    }

    @Test
    public void validPlaceBet() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("300");
        player.setBalance(Integer.parseInt(tv.getTester()));
        player.setBet(c.placeBet());
        assertEquals(300, player.getBet());
    }




    @Test(expected = IllegalArgumentException.class)
    public void determinePlayerMoveWithNonCommand() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("not a command");
        c.determinePlayerMove();
    }

    @Test (expected = IllegalArgumentException.class)
    public void determinePlayerMove3() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("quit");
        c.determinePlayerMove();
    }

    @Test (expected = IllegalArgumentException.class)
    public void determinePlayerDoubleMoveLowPlayerScore() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(1);
        tv.setTester("500");
        tv.setMoveInput("double");
        c.determinePlayerMove();
    }

    @Test (expected = IllegalArgumentException.class)
    public void determinePlayerDoubleMoveHighPlayerScore() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(12);
        tv.setTester("500");
        tv.setMoveInput("double");
        c.determinePlayerMove();
    }

    @Test
    public void determinePlayerDoubleMove() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(10);
        player.hand.addCard(new Card(1,1));
        player.hand.addCard(new Card(1,1));
        player.setBalance(10);
        tv.setTester("500");
        tv.setMoveInput("double");
        c.determinePlayerMove();
    }

    @Test
    public void determinePlayerStandMove() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setBalance(10);
        tv.setTester("500");
        tv.setMoveInput("stand");
        c.determinePlayerMove();
        assertTrue(c.isPlayerStands());
    }

    @Test
    public void determinePlayerHitMove() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setBalance(10);
        tv.setTester("500");
        tv.setMoveInput("hit");
        c.determinePlayerMove();
    }




    @Test
    public void playBlackJack(){
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("25");
        tv.setMoveInput("quit");
        c.playBlackjack();
    }

    @Test
    public void playBlackJackWithDone(){
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.done = true;
        tv.setTester("25");
        tv.setMoveInput("quit");
        c.playBlackjack();
    }

    @Test
    public void playBlackJackWithRound2(){
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setRoundNum(2);
        tv.setTester("25");
        tv.setMoveInput("quit");
        c.playBlackjack();
    }



    @Test(expected = TimeoutException.class)
    public void playBlackjackWithInvalidChipAmount() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("1");
        c.playBlackjack();
    }

    @Test(expected = TimeoutException.class)
    public void playBlackjackWithInvalidChipValue() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("a");
        c.playBlackjack();
    }



    @Test(expected = TimeoutException.class)
    public void setTheBetWithInvalidAmount() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("1");
        tv.setMoveInput("not a valid move");
        c.setTheBet();
    }

    @Test(expected = TimeoutException.class)
    public void setTheBetWithInvalidValue() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("a");
        tv.setMoveInput("not a valid move");
        c.setTheBet();
    }

    @Test //(expected = TimeoutException.class)
    public void determineTheMove() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("a");
        tv.setMoveInput("a");
        c.determineTheMove();
    }



    @Test (expected = IllegalArgumentException.class)
    public void testFailDouble() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("double");
        c.determinePlayerMove();
    }

    @Test
    public void playerStandOrBust_stand() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setMoveInput("quit");
        player.setScore(16);
        c.playerStandOrBust();
    }

    @Test
    public void playerStandOrBust_bust() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setBalance(350);
        tv.setTester("300");
        tv.setMoveInput("quit");
        player.setScore(22);
        c.playerStandOrBust();
    }

    @Test
    public void playerStandOrBust_else() {
        Player player = new Player();
        Dealer dealer = new Dealer();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setBalance(350);
        tv.setTester("300");
        tv.setMoveInput("quit");
        player.setScore(21);
        c.playerStandOrBust();
    }

    @Test
    public void determinePlayerScoreWithCards10AndBelow() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.hand.addCard(new Card(1, 3));
        player.hand.addCard(new Card(1, 2));
        player.hand.addCard(new Card(1, 10));
        assertEquals(15, c.determinePlayerScore());
    }

    @Test
    public void determinePlayerScoreWithAce() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.hand.addCard(new Card(1, 1));
        assertEquals(11, c.determinePlayerScore());
    }


    @Test
    public void determinePlayerScoreWithAceAndAbove21() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.hand.addCard(new Card(1, 11));
        player.hand.addCard(new Card(1, 12));
        player.hand.addCard(new Card(1, 13));
        player.hand.addCard(new Card(1, 1));

        assertEquals(31, c.determinePlayerScore());
    }

    @Test
    public void determinePlayerScoreWithJack() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.hand.addCard(new Card(1, 11));
        assertEquals(10, c.determinePlayerScore());
    }

    @Test
    public void determinePlayerScoreWithQueen() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.hand.addCard(new Card(1, 12));
        assertEquals(10, c.determinePlayerScore());
    }

    @Test
    public void determinePlayerScoreWithKing() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.hand.addCard(new Card(1, 13));
        assertEquals(10, c.determinePlayerScore());
    }



    @Test
    public void determineDealerScoreWithCards10AndBelow() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        dealer.hand.addCard(new Card(1, 3));
        dealer.hand.addCard(new Card(1, 2));
        dealer.hand.addCard(new Card(1, 10));
        assertEquals(15, c.determineDealerScore());
    }

    @Test
    public void determineDealerScoreWithAce() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        dealer.hand.addCard(new Card(1, 1));
        assertEquals(11, c.determineDealerScore());
    }

    @Test
    public void determineDealerScoreWithAceAndAbove21() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        dealer.hand.addCard(new Card(1, 11));
        dealer.hand.addCard(new Card(1, 12));
        dealer.hand.addCard(new Card(1, 13));
        dealer.hand.addCard(new Card(1, 1));

        assertEquals(31, c.determineDealerScore());
    }

    @Test
    public void determineDealerScoreWithJack() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        dealer.hand.addCard(new Card(1, 11));
        assertEquals(10, c.determineDealerScore());
    }

    @Test
    public void determineDealerScoreWithQueen() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        dealer.hand.addCard(new Card(1, 12));
        assertEquals(10, c.determineDealerScore());
    }

    @Test
    public void determineDealerScoreWithKing() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        dealer.hand.addCard(new Card(1, 13));
        assertEquals(10, c.determineDealerScore());
    }

    @Test
    public void determinePlayerWin() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(21);
        dealer.setScore(20);
        assertEquals(1,c.determineWinner());
    }

    @Test
    public void determinePlayerWinWithDealerBust() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(21);
        dealer.setScore(22);
        assertEquals(1,c.determineWinner());
    }

    @Test
    public void determinePlayerLossWithBothBust() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(22);
        dealer.setScore(24);
        assertEquals(0,c.determineWinner());
    }

    @Test
    public void determinePlayerLossWithTie() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(20);
        dealer.setScore(20);
        assertEquals(0,c.determineWinner());
    }

    @Test
    public void determineWinnerWithNeg() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(-1);
        dealer.setScore(5);
        assertEquals(0,c.determineWinner());
    }

    @Test
    public void determineWinnerWithD21P20() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(20);
        dealer.setScore(21);
        assertEquals(0,c.determineWinner());
    }

    @Test
    public void determineWinnerWithD22P20() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(19);
        dealer.setScore(20);
        assertEquals(0,c.determineWinner());
    }

    @Test
    public void determineGainsOnWin() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(20);
        dealer.setScore(18);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(1100, player.getBalance());
    }

    @Test
    public void determineLossesOnFail() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setSettingBet(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(15);
        dealer.setScore(18);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(900, player.getBalance());
    }

    @Test
    public void determineWinnerWithElse() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        player.setScore(-25);
        dealer.setScore(-27);
        assertEquals(0,c.determineWinner());
    }

    @Test
    public void determineWinnerWithDoubleAndPlayerGreaterScore() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(20);
        dealer.setScore(18);
        c.setTheBet();
        assertEquals(1, c.determineWinner());
    }

    @Test
    public void determineWinnerWithDoubleAndDealerBust() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(20);
        dealer.setScore(25);
        tv.setTester("100");
        c.setTheBet();
        assertEquals(1, c.determineWinner());
    }

    @Test
    public void dealerHit() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerStands(true);
        c.dealerHit();
        assertTrue(dealer.score>=17);
    }

    @Test //(expected = NumberFormatException.class)
    public void setTheBet() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        tv.setTester("a");
    }

    @Test
    public void determineLossesOnFailedDouble() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(15);
        dealer.setScore(20);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(800, player.getBalance());
    }

    @Test
    public void determineLossesOnBustedDouble() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(33);
        dealer.setScore(17);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(800, player.getBalance());
    }

    @Test
    public void determineLossesOnFailedDoubleDealer21() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(18);
        dealer.setScore(21);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(800, player.getBalance());
    }

    @Test
    public void determineLossesOnFailedDoubleTie() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(18);
        dealer.setScore(18);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(800, player.getBalance());
    }

    @Test
    public void determineGainsOnDoubleDealerBust() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(3000);
        player.setBet(100);
        player.setScore(18);
        dealer.setScore(25);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(3200, player.getBalance());
    }

    @Test
    public void determineGainsOnDoubleGreaterThanDealer() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        player.setBalance(1000);
        player.setBet(100);
        player.setScore(18);
        dealer.setScore(15);
        tv.setTester("100");
        c.setTheBet();
        c.determineWinner();
        assertEquals(1200, player.getBalance());
    }

    @Test
    public void buyingChipsFlag() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setBuyingChips(true);
        assertTrue(c.isBuyingChips());
    }

    @Test
    public void settingBetFlag() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setSettingBet(true);
        assertTrue(c.isSettingBet());
    }

    @Test
    public void determiningMoveFlag() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setDeterminingMove(true);
        assertTrue(c.isDeterminingMove());
    }

    @Test
    public void playerStandsFlag() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerStands(true);
        assertTrue(c.isPlayerStands());
    }

    @Test
    public void playerDoubleFlag() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setPlayerDouble(true);
        assertTrue(c.isPlayerDouble());
    }

    @Test
    public void roundNumCounter() {
        Dealer dealer = new Dealer();
        Player player = new Player();
        TestView tv = new TestView();
        Controller c = new Controller(player, dealer, tv);
        c.setRoundNum(2);
        assertEquals(2,c.getRoundNum());
    }


}